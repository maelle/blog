---
title: "Frie Preu"
image: "images/frie.jpg"
links:
  - label: Blog
    url: "blog.html"
  - label: Skills
    url: "about.html#skills"
  - label: R Projects
    url: "r.html"
  - label: Twitter
    url: "https://twitter.com/ameisen_strasse"
  - label: GitHub
    url: "https://github.com/friep"
  - label: GitLab
    url: "https://gitlab.com/friep/"
  - label: LinkedIn
    url: "https://linkedin.com/in/friedrike-preu-a2bb46a7/"
output:
  postcards::jolla
site: distill::distill_website
---
I love wrangling data, building tools and automating processes. 
 
low-budget data engineer - data scientist - data4gooder - software developer.
