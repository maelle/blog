---
title: "About me"
description: |
  Some things about me, myself and I.
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(fontawesome)
```

Hello! `r emo::ji("wave")`

My name is Frie^[Pronounced like the word "free" if you were wondering. Yeah, that made for some funny situations during my Erasmus year in the UK.], I'm a data scientist / low-budget data engineer^[I heavily use GitHub actions, R and all sorts of other shenanigans to automate data-related processes. It's all on a small scale but I really enjoy this sort of work.] / coding person from Germany. Until the end of 2021, I work as COO for [CorrelAid](https://correlaid.org), a data4good network of over 1500 data scientists where I am  in charge of our project coordination and developing tools and maintaining our technical infrastructure. In addition, I (co-)organize educational events such as the [CorrelCon](https://docs.correlaid.org/correlcollection/correlcon), the monthly [Open Online Data Meetup](https://docs.correlaid.org/correlcollection/oodm) and the biweekly CorrelAid [tidytuesday coding hangout](https://tidytuesday.correlaid.org/). Before that, I studied political science and data science and worked in IT consulting for two years. Besides that, I also enjoy being outdoors (hiking, exploring), being indoors (being lazy), taking photos of things and eating good food. I am quite active on [Twitter](https://twitter.com/ameisen_strasse/), so feel free to follow me there.

You can find out about **some** of my projects under the projects menu. 


## Skills and interests {#skills}
- **R** for data related work: I have been using R since 2014 and whenever it makes sense, I use it for my projects. I mostly develop R packages and write RMarkdown reports for internal use at CorrelAid, automate boring stuff I am too lazy for (see my [blog](index.html) and the [R projects](r.html) page), collect data via APIs or webscraping and juggle around data with the tidyverse stack. Since autumn 2020, I also participate in #tidytuesday (see the CorrelAid [tidytuesday blog](https://tidytuesday.correlaid.org/)) and have some experience with developing Shiny apps.
- **Python** for backend development and data science applications. I heavily used Python for (data science) backend development at codecentric and it's still my go-to if I have to develop things like APIs (e.g. with fastapi). 
- **Git**: first rule of programming: always use version control. I also like [teaching Git](gost.html).
- **Docker**: second rule of programming: always dockerize. Rule 2a.: Use docker-compose. 
- **GitHub actions / GitLab CI/CD**: I enjoy automating things. Of course, I use CI/CD for standard use cases such as running the tests for my R packages but I also abuse it in various other ways, e.g. to [notify me about failure](https://github.com/friep/correlaid-utils/actions) on a Raspberry Pi :) 
- **Node.js**: I have experience working with Node.js from a project at codecentric and from developing a [Slackbot for CorrelAid](https://github.com/CorrelAid/correlaid-slackbot-js).
- **Vue.js**: I'm not a very good frontend dev but I can build small things in Vue.js - thanks to Jan Dix for teaching me! For example, together with Jan and as part of a CorrelAid project, I co-developed a [database](https://www.umweltprofis.ch/datenbank) and "[assessment](https://www.umweltprofis.ch/assessment)" for a Swiss NPO promoting job options in the sustainability field.
- **AWS Lambda and serverless**: I have used AWS Lambda + serverless for a project at codecentric and the [Slackbot for CorrelAid](https://github.com/CorrelAid/correlaid-slackbot-js).
- **Unix/Linux and Shell scripting**
- **Elastic stack**: I have used the Elastic stack in two projects at codecentric.
- **server admin**: as the main "sysadmin" for CorrelAid, I have experience with setting up and maintaining virtual machines and deploying applications on them using tools like Docker, Nginx and traefik. For example, I have deployed [pad.correlaid.org](https://pad.correlaid.org) which I also integrated with Azure AD. 

## Follow me {#followme}
- `r fontawesome::fa("github")` [friep](https://github.com/friep/)
- `r fontawesome::fa("gitlab")` [friep](https://gitlab.com/friep/)
- `r fontawesome::fa("twitter")` [ameisen_strasse](https://twitter.com/ameisen_strasse/)^["ant route" (literally "ant street") in German. `r emo::ji("ant")` The story behind that is that I'm highly uncreative and when I created my Twitter account I was laying on my bed and there was an "ant street" next to my bedstand. Yes, really. It was 2013 and I was living in the basement of a Berlin family during my internship.]
- `r fontawesome::fa("linkedin")` [Frie Preu](https://linkedin.com/in/friedrike-preu-a2bb46a7/)
- `r fontawesome::fa("keybase")` [friep](https://keybase.io/friep)
